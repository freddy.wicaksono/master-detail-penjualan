    <div class="sidebar-header">
        <div>
          
          <img <img src="{{ asset('assets/images/logo-icon-2.png') }}" class="logo-icon" alt="Your Image">
        </div>
        <div>
          <h4 class="logo-text">Fobia</h4>
        </div>
    </div>
      <!--navigation-->
      <ul class="metismenu" id="menu">
      <li class="menu-label">Menu Utama</li>
        <li>
          <a href="/">
            <div class="parent-icon">
              <ion-icon name="home-outline"></ion-icon>
            </div>
            <div class="menu-title">Dashboard</div>
          </a>
        </li>
        <li>
          <a href="/barang">
            <div class="parent-icon">
              <ion-icon name="bag-handle-outline"></ion-icon>
            </div>
            <div class="menu-title">Barang</div>
          </a>
        </li>
        
        <li>
          <a href="/penjualan">
            <div class="parent-icon">
              <ion-icon name="gift-outline"></ion-icon>
            </div>
            <div class="menu-title">Penjualan</div>
          </a>
        </li>
        
      </ul>