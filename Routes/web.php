<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('barang', BarangController::class);
Route::resource('penjualan', PenjualanController::class);

// Create: Show the form to create a new penjualandetail
Route::get('penjualandetail/{id}/create', [PenjualandetailController::class, 'create'])->name('penjualandetail.create');

// Store: Store a newly created penjualandetail in the database
Route::post('penjualandetail', [PenjualandetailController::class, 'store'])->name('penjualandetail.store');

// Show: Display a specific penjualandetail
Route::get('penjualandetail/{id}/list', [PenjualandetailController::class, 'list'])->name('penjualandetail.list');

// Destroy: Delete a specific penjualandetail from the database
Route::delete('penjualandetail/{detail_id}/delete/{penjualan_id}', [PenjualandetailController::class, 'destroy'])->name('penjualandetail.destroy');

// Route to handle the AJAX search request
Route::get('/search-barang', [BarangController::class, 'search'])->name('search.barang');

Route::get('penjualandetail/{id}/lunas', [PenjualandetailController::class, 'setLunas'])->name('penjualandetail.lunas');