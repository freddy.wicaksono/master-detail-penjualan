<?php
/*
 Nama file: App/Models/BarangModel.php
 Tools : LaravelGhost v1
 Created By : Freddy Wicaksono, M.Kom
 Tanggal : 17-Jun-2024
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class BarangModel extends Model
{
    use HasFactory;
    protected $table = 'barang';    
    public $timestamps = false;
    protected $fillable = ['kode_barang','nama_barang','harga','kategori'];

}