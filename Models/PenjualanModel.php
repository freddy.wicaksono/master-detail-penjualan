<?php
/*
 Nama file: App/Models/PenjualanModel.php
 Tools : LaravelGhost v1
 Created By : Freddy Wicaksono, M.Kom
 Tanggal : 17-Jun-2024
*/
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class PenjualanModel extends Model
{
    use HasFactory;
    protected $table = 'penjualan';    
    public $timestamps = false;
    protected $fillable = ['nomor_bukti','tanggal','jenis_pembayaran','noktp','uraian','total_pembelian','uang_muka','sisa_pembayaran','status_pembayaran'];

    public function penjualandetail()
    {
        return $this->hasMany(PenjualandetailModel::class, 'penjualan_id');
    }
}